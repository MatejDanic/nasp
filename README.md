# NASP

Laboratorij iz predmeta Napredni algoritmi i strukture podataka

## 1. laboratorijska vježba

### Matej Đanić 0036500396

### Zagreb, 4.11.


## 1. Zadatak

1) Napisati program koji učitava niz prirodnih brojeva iz ASCII datoteke (po
pretpostavci, datoteka nije prazna) i upisuje ih u (inicijalno prazno) AVL stablo istim
redoslijedom kao u datoteci. Program može biti konzolni ili s grafičkim sučeljem, po
vlastitom izboru. Konzolni program naziv ulazne datoteke treba primiti prilikom
pokretanja kao (jedini) argument s komandne linije, a grafički iz odgovarajućeg
sučelja po pokretanju programa. Nakon upisa svih podataka, ispisati izgrađeno
stablo na standardni izlaz (monitor). Program zatim treba omogućiti dodavanje novih
čvorova te nakon svake promjene treba ponovo ispisati stablo.

Napomena: datoteka treba biti tekstualna, a brojevi u datoteci odvojeni po jednim
razmakom (space). Kraj datoteke treba biti označen standardnim EOF znakom.

### Isto kao zadatci za 11 bodova, samo treba dodati i brisanje čvora iz stabla.

## 2. Rješenje zadatka

### 2.1. Teorijski uvod

Cilj AVL algoritma je uravnotežavanje binarnih stabala. Algoritam se bazira na
dinamičkim restrukturiranjem prilikom dodavanja i/ili brisanja podataka iz stabla[1].
AVL algoritam primjenjuje samo lokalno uravnoteženje. Glavno pravilo kojeg se
algoritam drži je da se visine podstabala pojedinog čvora ne smiju razlikovati za više
od 1.
Pri dodavanju podatka u stablo prvo se pronalazi mjesto u kojem bi stablo s
novim elementom zadovoljavalo svojstvo binarnog stabla. Zatim se računa razlika
visina (faktor ravnoteže) podstabala svih čvorova počevši od novog čvora do
korijena. Ako je ta razlika 0, stablo je uravnoteženo, ako iznosi +1/-1 potrebno se
prebaciti na čvor roditelj i nastaviti ažuriranje. Ukoliko je razlika visina podstabala na
čvoru veća od 1 ili manja od - 1 , pokreće se postupak uravnotežavanja.
Slično kao kod dodavanja podatka u stablo, prilikom brisanja podatka, računa se
faktor ravnoteže od mjesta s kojeg je uklonjen čvor do korijena te se ovisno o iznosu
izvršava uravnotežavanje stabla.

### 2.2. Implementacija

Zadatak se implementira u programskom jeziku Java, koristeći razvojno okruženje
Eclipse. Program je konzolni, a kao jedini parametar pri pokretanju se predaje naziv
datoteke iz koje će učitati prirodne brojeve. Datoteka je tekstualna i sadrži brojeve
odvojene razmakom. Nakon unosa svih brojeva u stablo, program omogućuje izbor
dodavanja ili brisanja podatka iz stabla te gašenje programa. Odabiranje pojedine
opcije ostvaruje se unosom brojeva u konzolu, 1 / 2 za dodavanje/brisanje te 0 za
gašenje programa.
Nakon odabira opcije dodavanja ili brisanja, potrebno je unijeti u konzolu broj koji
se želi dodati ili ozbrisati iz stabla. Nakon svakog ažuriranj, ispisuje se cijelo stablo
kako bi se utvrdila uspješnost akcije. Postupak se ponavlja dok se ne odabere opcija
gašenja.


Svaki čvor implementiran je klasom Node koja sadrži svoje lijevo i desno dijete,
podatak i visinu na kojoj se nalazi. Pri stvaranju objekta čvora, djeca se postavljaju
na null, a visina na 1.

#### 2.2.1. Dodavanje čvora

Dodavanje novog čvora u AVL stablo izvršava se u dva osnovna koraka:

1. Pronalaženje mjesta u trenutnoj strukturi stabla i dodavanje.
2. Uravnotežavanje stabla

Rekurzivna funkcija za dodavanje čvora addNode:

```
addNode(node, data):
if node == null then
create node with data;
end if
```
```
if node.data == data then
ignore duplicate;
else if node.data > data then
addNode(node.leftChild, data)
else
addNode(node.rightChild, data)
end if
```
```
node.height = 1 + maximum height between left and right child;
```
```
balanceFactor = node.leftChild height – node.rightChild height
if balanceFactor > 1 or < - 1 then
balance node;
end if
return node;
```
Postupak uravnotežavanja čvora nakon dodavanja ovisi o njegovom faktoru
ravnoteže i odnosu nove vrijednosti i vrijednosti pohranjenoj u čvoru, a može se
podijeliti na 2 skupine:

1. Faktor ravnoteže veći od 1
    a. Nova vrijednost manja od vrijednosti u čvoru:
       Desna rotacija s čvorom kao korijenom
    b. Nova vrijednost veća od vrijednosti u čvoru:
       Lijeva rotacija s lijevim djetetom čvora kao korijenom
       Desna rotacija s čvorom kao korijenom
2. Faktor ravnoteže manji od - 1
    a. Nova vrijednost manja od vrijednosti u čvoru:
       Desna rotacija s desnim djetetom čvora kao korijenom
       Lijeva rotacija s čvorom kao korijenom
    b. Nova vrijednost veća od vrijednosti u čvoru:
       Lijeva rotacija s čvorom kao korijenom

#### 2.2.2. Brisanje čvora

Brisanje čvora iz AVL stabla također se izvršava u dva koraka:


1. Pronalaženje i uklanjanje čvora metodom Deletion by Copying
2. Uravnotežavanje stabla

Rekurzivna funkcija za brisanje čvora removeNode:

```
removeNode(node, data):
if node == null then
node with data doesn't exist in tree;
end if
```
```
if node.data == data then
delete by copying;
else if node.data > data then
removeNode(node.leftChild, data)
else
removeNode(node.rightChild, data)
end if
```
```
node.height = 1 + maximum height between left and right child;
```
balanceFactor = node.leftChild height – node.rightChild height
if balanceFactor > 1 or < - 1 then
balance node;
end if
return node;

#### 2.2.3. Uravnotežavanje

U prethodne dvije funkcionalnosti nakon dodavanja/brisanja čvora, izvršava se
uravnotežavanje ako je faktor ravnoteže za trenutni čvor veći od 1 ili manji od -1. To
se postiže odgovarajućim funkcijama rotiranja čvorova. Ako je faktor ravnoteže veći
od 1 znači da je lijevo podstablo više od desnog, a ako je manji od -1 obrnuto.

Funkcija rotationLeft:
rotationLeft(node):
temp = node.rightChild;
temp 2 = temp.leftChild;

```
temp.leftChild = node;
node.rightChild = temp2;
```
```
node.height = 1 + maximum height between left and right child;
temp.height = 1 + maximum height between left and right child;
return temp;
```
Funkcija rotationRight:
rotationRight(node):
temp = node.leftChild;
temp 2 = temp.rightChild;

```
temp.rightChild = node;
node.leftChild = temp2;
```
```
node.height = 1 + maximum height between left and right child;
temp.height = 1 + maximum height between left and right child;
```
Postupak uravnotežavanja čvora nakon brisanja ovisi o njegovom faktoru
ravnoteže i odnosu nove vrijednosti i vrijednosti faktora ravnoteže lijevog i desnog
djeteta te se također može podijeliti na 2 skupine:


1. Faktor ravnoteže veći od 1
    a. Faktor ravnoteže lijevog djeteta veći ili jednak od 0:
       Desna rotacija s čvorom kao korijenom
    b. Faktor ravnoteže lijevog djeteta manji od 0:
       Lijeva rotacija s lijevim djetetom čvora kao korijenom
       Desna rotacija s čvorom kao korijenom
2. Faktor ravnoteže manji od - 1
    a. Faktor ravnoteže lijevog djeteta veći ili jednak od 0 :
       Desna rotacija s desnim djetetom čvora kao korijenom
       Lijeva rotacija s čvorom kao korijenom
    b. Faktor ravnoteže lijevog djeteta manji od 0:
       Lijeva rotacija s čvorom kao korijenom

## 3. Zaključak

AVL algoritam je dobra alternativa dobivanja uravnoteženog stabla promišljenim
redoslijedom upisa podataka, tj. prethodne pripreme podataka. AVL algoritam
dinamički restrukturira stablo nakon ažuriranja (dodavanja ili brisanja podatka), što
nam omogućava unos podataka koji nisu sortirani te ih je moguće naknadno
dodavati. Ovo je velika prednost iz razloga što se u stvarnosti, situacije gdje sve
podatke posjedujemo tijekom same inicijalizacije stabla, rijetko događaju.
Moguća poboljšanja izrađenog programa uključuju grafičko sučelje za odabir
opcija dodavanja ili brisanja čvorova te estetički bolji ispis postojećeg stabla.

## 4. Literatura

[1] Nikica Hlupić and Damir Kalpić, Napredne strukture podataka (Advanced Data
Structures), 2009


