import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Paths;
import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {
		File file = new File(Paths.get("").toAbsolutePath().toString() + "\\" + args[0]);
		BufferedReader br;
		String line = "";
		try {
			br = new BufferedReader(new FileReader(file));
			line = br.readLine();
		} catch (Exception e) {
			e.printStackTrace();
		}
		String[] numberFlow = line.split("\\s+");
		AVLTree tree = new AVLTree();
		System.out.println(tree.printTree(tree.root, 0));

		for (String number : numberFlow) {
			tree.root = tree.addNode(tree.root, Integer.parseInt(number));
		}
		System.out.println(tree.printTree(tree.root, 0));
		Scanner input = new Scanner(System.in);
		while(true) {
			System.out.println("0 - EXIT\n1 - ADD NODE\n2 - DELETE NODE");
			int i = input.nextInt();
			if (i == 0) {
				System.out.println("---END---");
				break;
			}
			else if (i == 1) {
				System.out.println("ENTER DATA");
				tree.root = tree.addNode(tree.root, input.nextInt());
			} else if (i == 2) {
				System.out.println("ENTER DATA");
				tree.root = tree.removeNode(tree.root, input.nextInt());
			}
			System.out.println(tree.printTree(tree.root, 0));
		}
		input.close();
		
	}
}
