public class AVLTree {
	Node root;
	
	public Node addNode(Node node, int data) {
		if (node == null) {
			System.out.println("Adding node " + data + " to tree.");
			return (new Node(data));
		}
//		System.out.println("\n" + node + "\n" + node.leftChild + "  " + node.rightChild +"\n");
		if (node.data == data) {
			System.out.println("Node with data " + data + " already exists.");
			return node;
		} else if (node.data > data) {
			node.leftChild = addNode(node.leftChild, data);
		} else {
			node.rightChild = addNode(node.rightChild, data);
		}
		node.height = 1 + maxHeight(node.leftChild, node.rightChild);
		

		// BALANCING
		int balance = getHeight(node.leftChild) - getHeight(node.rightChild);
		if (balance > 1) {
			if (data < node.leftChild.data) {
				node = rotationRight(node);
			} else {
				node.leftChild = rotationLeft(node.leftChild);
				node = rotationRight(node);
			}
		} else if (balance < -1){
			if (data < node.rightChild.data) {
				node.rightChild = rotationRight(node.rightChild);
				node = rotationLeft(node);
			} else {
				node = rotationLeft(node);
			}
		}
		return node;
	}
	
	public Node removeNode(Node node, int data) {
//		System.out.println(node + " " + data);
		if (node == null) {
			System.out.println("Node with data " + data + " doesn't exist within tree.");
			return node;
		}
		if (node.data == data) {
			System.out.println("Removing node " + node + " from tree.");
			if ((node.leftChild == null) || (node.rightChild == null)) {
				Node temp = null;
				if (temp != node.rightChild) {
					temp = node.rightChild;
				} else {
					temp = node.leftChild;
				}
				if (temp == null) {
					temp = node;
					node = null;
				} else {
					node = temp;
				}
			} else {
				Node temp = minValueNode(node.rightChild);
				System.out.println("Replacing node " + node + " with " + temp + ".");
				node.data = temp.data;
				node.rightChild = removeNode(node.rightChild, temp.data);
			}
		} else if (node.data > data) {
			node.leftChild = removeNode(node.leftChild, data);
		} else {
			node.rightChild = removeNode(node.rightChild, data);
		}
//		System.out.println(node + " " + data);
		if (node != null) {
			node.height = maxHeight(node.leftChild, node.rightChild) + 1;
			
			
			// BALANCING
			int balance = getHeight(node.leftChild) - getHeight(node.rightChild);
			int balanceLeft = 0;
			int balanceRight = 0;
			if (node.leftChild != null) balanceLeft = getHeight(node.leftChild.leftChild) - getHeight(node.leftChild.rightChild);
			if (node.rightChild != null) balanceRight = getHeight(node.rightChild.leftChild) - getHeight(node.rightChild.rightChild);
			if (balance > 1) {
				if (balanceLeft >= 0) {
					node = rotationRight(node);
				} else {
					node.leftChild = rotationLeft(node.leftChild);
					node = rotationRight(node);
				}
			} else if (balance < -1){
				if (balanceRight > 0) {
					node.rightChild = rotationRight(node.rightChild);
					node = rotationLeft(node);
				} else {
					node = rotationLeft(node);
				}
			}
		}
		return node;
	}

	private Node minValueNode(Node node) {
		Node minNode = node;
		while (minNode.leftChild != null) {
			minNode = minNode.leftChild;
		}
		return minNode;
	}

	int maxHeight(Node x, Node y) {
        return (getHeight(x) > getHeight(y) ? getHeight(x) : getHeight(y));
    }
	
	private Node rotationLeft(Node node) {
//		System.out.println("prva rotacija\n" + print(node, 0));
		Node temp = node.rightChild;
		Node temp2 = temp.leftChild;

		temp.leftChild = node;
		node.rightChild = temp2;
		
		node.height = maxHeight(node.leftChild, node.rightChild) + 1;
		temp.height = maxHeight(temp.leftChild, temp.rightChild) + 1;
//		System.out.println("kraj prve rotacija\n" + print(temp, 0));
		return temp;
	}

	private Node rotationRight(Node node) {
//		System.out.println("prva rotacija\n" + print(node, 0));
		Node temp = node.leftChild;
		Node temp2 = temp.rightChild;
		
		temp.rightChild = node;
		node.leftChild = temp2;

		node.height = maxHeight(node.leftChild, node.rightChild) + 1;
		temp.height = maxHeight(temp.leftChild, temp.rightChild) + 1;
//		System.out.println("kraj prve rotacije\n" + print(temp, 0));
		return temp;
	}

	public String printTree(Node node, int depth) {
		String result = "";
		if (depth == 0) result = "\nTree:\n";
		for (int i = 0; i < depth; i++) {
			result += "____";
		}
		if (node == null) {
			result += /* getHeight(node) + */ "(-)\n";
//			result = "";
			return result;
		}
		return result + node + "\n" + printTree(node.leftChild, depth + 1) + printTree(node.rightChild, depth + 1);
	}
	
	public int getHeight(Node node) {
		int height = 0;
		if (node != null) height = node.height;
		return height;
	}
}
