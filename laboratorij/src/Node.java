public class Node {
	
	Node leftChild, rightChild;
	int data;
	int height;
	
	public Node(int data) {
		this.data = data;
		this.leftChild = null;
		this.rightChild = null;
		this.height = 1;
	}

	@Override
	public String toString() {
		return /*Integer.toString(this.height) + */ "(" + Integer.toString(this.data) + ")";
	}
}
